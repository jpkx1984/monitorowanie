#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include <Arduino.h>
#include <HardwareSerial.h>

extern "C" {
#include "hd44780.h"
}

/*
 * konfiguracja PINów
 * usart0
 * tx - pd0
 * rx - pd1
 * xck - pd4
 *
 * ADC
 * aref
 * PC0 ADC0 - in
 *
 * LCD
 * D4 - C1 - 24
 * D5 - C2 - 25
 * D6 - C3 - 26
 * D7 - C4 - 27
 *
 * en - B0 - 14
 * RW - B1 - 15
 * RS - B2 - 16
 */

// wartość po przetworzeniu przez ADC
volatile uint16_t adc_val = 0;

// liczba wywołań przerwania ADC
volatile uint32_t adc_count = 0;
uint32_t sin_tab[100];
volatile bool direction = true;

ISR(ADC_vect)
{
    ++adc_count;
    // na symulowanym systemie nie działa przekazywanie danych o napięciu na pinie analogowym, zawsze jest 0
    //adc_val = (ADCH<<8) | ADCL;
    if (adc_count > 10000) {
        if (direction && adc_val == 200) {
            direction = false;
        } else if (!direction && adc_val == 0) {
            direction = true;
        }

        adc_val = direction ? adc_val+1 : adc_val - 1;

        adc_count = 0;
    }
    ADCSRA |= 1<<ADSC;
}

void adc_init(void)
{
    DDRC &= 0b11111110;
    PRR &= ~ _BV(PRADC);
    ADMUX = 0b00000000;
    ADCSRB &= 0b11111000;
    ADCSRA |= 0b11101111;
}


void setup() {
    cli();

    // inicjalizacja rejestrów ADC
    adc_init();
    sei();

    // inicjalizacja UARTa
    Serial.begin(9600);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    // inicjalizacja sterownika LCD
    lcd_init();
    delay(100);
    lcd_clrscr();
}


void loop() {
    uint16_t v = adc_val;
    char buff[200];

    // wypisanie aktualnej wartości na ekran LCD
    sprintf(buff, "Val: %u", v);
    lcd_clrscr();
    lcd_home();
    lcd_puts(buff);

    // wyrzucenie danych na wyjście szeregowe w formacie: <wartość><koniec linii>
    if (Serial && Serial.availableForWrite()) {
        Serial.println(v);
    }

    delay(2000);
}
