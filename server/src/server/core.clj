(ns server.core
  (:require [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]])
  (:import
   [java.text NumberFormat SimpleDateFormat]
   [java.util Locale]
   [java.time Instant]
   [java.time.temporal ChronoField]
   [com.google.api.client.auth.oauth2 Credential]
   [com.google.api.client.googleapis.javanet GoogleNetHttpTransport]
   [com.google.api.client.json JsonFactory]
   [com.google.api.client.json.jackson2 JacksonFactory]
   [com.google.api.client.extensions.jetty.auth.oauth2 LocalServerReceiver LocalServerReceiver$Builder]
   [com.google.api.client.googleapis.auth.oauth2 GoogleClientSecrets GoogleAuthorizationCodeFlow GoogleAuthorizationCodeFlow$Builder]
   [com.google.api.services.sheets.v4 SheetsScopes Sheets Sheets$Builder]
   [com.google.api.client.extensions.java6.auth.oauth2 AuthorizationCodeInstalledApp]
   [com.google.api.client.util.store FileDataStoreFactory]
   [com.google.api.services.sheets.v4.model ValueRange])
  (:gen-class))

(def HTTP_TRANSPORT (GoogleNetHttpTransport/newTrustedTransport))
(def DATA_STORE_DIR "/home/janusz/Dokumenty/google_api/data_sucker")
(def DATA_STORE_FACTORY (FileDataStoreFactory. (io/file DATA_STORE_DIR)))
(def JSON_FACTORY (JacksonFactory/getDefaultInstance))

(def date-format (SimpleDateFormat. "yyyy-MM-dd hh:mm:ss"))
(defn format-time [d]
  (.format date-format d))

(def ^:dynamic *secret-path*)

(defn authorize []
  "Autentykacja."
  (with-open [client-secret-reader
              (io/reader "/home/janusz/Dokumenty/google_api/data_sucker.json")]
    (let [client-secret (GoogleClientSecrets/load JSON_FACTORY client-secret-reader)
          scopes [SheetsScopes/SPREADSHEETS]
          flow (.. (GoogleAuthorizationCodeFlow$Builder.
                    HTTP_TRANSPORT
                    JSON_FACTORY
                    client-secret
                    scopes)
                   (setDataStoreFactory DATA_STORE_FACTORY)
                   (setAccessType "offline")
                   (build))]
      (.authorize (AuthorizationCodeInstalledApp. flow
                                                  (.. (LocalServerReceiver$Builder.)
                                                      (setPort 47000)
                                                      (build))) "user"))))

(defn fetch-sheets-service []
  (.. (Sheets$Builder. HTTP_TRANSPORT JSON_FACTORY (authorize))
      (setApplicationName "data_sucker")
      (build)))

(def ^:dynamic *sheets-service* (fetch-sheets-service))

;; arkusz
(def ^:dynamic *doc-id* "1ISENevw5EJFCsqVI9tVWY-SB5X0hBl_N0OAj_iYO99s")

;; zakładka
(def ^:dynamic *tab-name* "Pomiary")

(defn put-values [data-stream]
  "Dodaj dane do arkusza, co ok. 5 sekund. Pośrednie dane są ignorowane."
  (let [doc-id *doc-id*
        data-range (str *tab-name*  "!A")
        service *sheets-service*
        previous-timestamp (atom nil)]
    ;; ustaw nazwy kolumn
    (.. service
        (spreadsheets)
        (values)
        (update doc-id (str data-range "1") (doto (ValueRange.)
                                              (.setRange (str data-range 1))
                                              (.setValues [["date" "value"]])))
        (setValueInputOption "RAW")
        (execute))
    ;; dodawaj sdane
    (doseq [ds data-stream]
      (let [timestamp (Instant/now)]
        ;; tylko co ok. 5 sekund
        (when (or (not @previous-timestamp)
                  (>= (- (.getEpochSecond timestamp) (.getEpochSecond @previous-timestamp)) 5))
          (let [dr (str *tab-name* "!A:B")
                nd (doto (ValueRange.)
                     (.setRange dr)
                     (.setValues [[(format-time (java.util.Date.)) (if-not (empty? ds) (Integer/parseInt ds) 0)]]))]
            (reset! previous-timestamp timestamp)
            ;; dopisz wiersz
            (.. service
                (spreadsheets)
                (values)
                (append doc-id dr nd)
                (setValueInputOption "RAW")
                (execute))))))))

(defn is-file? [^String f]
  "Czy ścieżka to plik"
  (try
    (let [f (io/file f)]
      (and (.exists f)
           (.isFile f)))
    (catch Exception e
      false)))

;; konfiguracja parsowania parametrów z linii komend
(def cli-options
  [["-f" "--fifo-path path" "Ścieżka do strumienia danych"
    :default "/tmp/simavr1-uart0"
    :parse-fn identity
    :validate [(.exists (io/file "Wymagana ścieżka")) "Plik musi istnieć"]]
   ["-t" "--tab tab" "Nazwa zakładki"
    :default "Arkusz1"
    :parse-fn identity
    :validate [(complement empty) "Nazwa nie może byc pusta"]]
   ["-d" "--document doc" "Nazwa arkusza"
    :default "1ISENevw5EJFCsqVI9tVWY-SB5X0hBl_N0OAj_iYO99s"
    :validate [(complement empty?) "Nazwa nie może byc pusta"]]
   ["-s" "--secret secret" "Ścieżka do klucza użytkownika"
    :default "/home/janusz/Dokumenty/google_api/data_sucker.json"
    :validate [is-file? "Wymagana ścieżka"]]
   ["-h" "--help"]])

(defn -main [& args]
  (let [{options :options
         errors :errors} (parse-opts args cli-options)
        {tab :tab
         fifo :fifo-path
         document :document
         secret :secret} options]
    (when-not errors
      (binding [*tab-name* tab
                *doc-id* document
                *secret-path* secret]
        (let [stream (io/reader fifo)
              lines (line-seq stream)]
          (put-values lines))))
    ;; wyświetl błędy
    (when errors
      (clojure.pprint/pprint errors))))
